import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const Categories = new Mongo.Collection('categories');

if (Meteor.isServer) {
    Meteor.publish('categories', () => {
        return Categories.find();
    });
}

Meteor.methods({
    'input.new-category'(name){
        check(name, String);

        //if not logged
        if(!Meteor.user()) throw new Meteor.Error('not-authorized');

        //insert
        Categories.insert({
            name,
            links: [],
            createdAt: new Date(),
            owner: Meteor.user().username
        });
    },
    'input.new-link'(url, description, categoryId){
        check(url, String);
        check(description, String);
        check(categoryId, String);

        //if not logged
        if(!Meteor.user()) throw new Meteor.Error('not-authorized');
        //if categoryId doesn't exists (may be a hack attempt)
        if(!Categories.findOne({_id: categoryId}))
            throw new Meteor.Error(`category doesn't exist`);

        //insert
        Categories.update({_id: categoryId},{
            $push: {
                links: {
                    _parentId: categoryId,
                    url: url,
                    description: description,
                    createdAt: new Date(),
                    owner: Meteor.user().username
                }
            }
        });
    },
    'category.remove'(id, owner){
        if(owner == Meteor.user().username)
            Categories.remove({_id: id});
    },
    'link.remove'(_parentId, url, owner){
        if(owner == Meteor.user().username)
            Categories.update(
                {_id: _parentId},
                { $pull: {
                    links: { url: url}
                }}
            );
    }
});
