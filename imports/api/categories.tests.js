/* eslint-env mocha */

import { Meteor } from 'meteor/meteor';
import { Random } from 'meteor/random';
import { assert } from 'meteor/meteortesting:mocha';

import { Categories } from './categories.js';

if (Meteor.isServer) {
    describe('Categories', () => {
        describe('methods', () => {
            const username =  "bidetaggle";
            let categoryId;

            beforeEach(() => {
                Categories.remove({});
                categoryId = Categories.insert({
                    name: 'name category',
                    links: [],
                    owner: username,
                    createdAt: new Date(),
                });
            });

            it('can delete owned task', () => {
                const deleteCategory = Meteor.server.method_handlers['category.remove'];
                const invocation = { username };
                deleteCategory.apply(invocation, [categoryId]);
                assert.equal(Tasks.find().count(), 0);
            });
        });
    });
}
