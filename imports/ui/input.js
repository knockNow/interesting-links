import { Template } from 'meteor/templating';
import { Random } from 'meteor/random';
import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import { Categories } from '../api/categories.js';
import './category.js';
import './link.js';
import './input.html';

Template.input.helpers({
    categoriesNumber() {
        return Categories.find().count();
    },
    categories() {
        return Categories.find({}, {sort: {createdAt: -1}});
    }
});

Template.input.events({
    'submit .new-category'(event) {
        event.preventDefault();

        const target = event.target;
        const name = target.name.value;

        //input verification
        if(Categories.findOne({name: name}) || name == ""){
            window.plugins.toast.showShortCenter('not valid dude!');
        }
        //insert
        else{
            Meteor.call('input.new-category', name);
            //clear form
            target.name.value = '';
        }

        // Close modale
        var modale = document.getElementById('showLink')
        modale.style.display = (modale.dataset.toggled ^= 1) ? "flex" : "none";

    },
    'submit .new-link'(event) {
        // Prevent default browser form submit
        event.preventDefault();

        // Get value from form element
        const target = event.target;

        const link = target.link.value;
        const description = target.description.value;
        const categoryId = target.category.value;

        //input verification
        if(description == "" || link == ""){
            window.plugins.toast.showShortCenter(`All the fields have to be filled dude`);
        }
        //insert
        else{
            Meteor.call('input.new-link', link, description, categoryId);
            // Clear form
            target.link.value = '';
            target.description.value = '';
        }

        // Close modale
        var modale = document.getElementById('showLink')
        modale.style.display = (modale.dataset.toggled ^= 1) ? "flex" : "none";

    },
});
