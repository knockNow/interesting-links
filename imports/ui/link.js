import { Template } from 'meteor/templating';
import { Meteor } from 'meteor/meteor';
import { Categories } from '../api/categories.js';
import './link.html';

Template.link.helpers({
    is_owner() {
        if(Meteor.user())
            return (this.owner === Meteor.user().username);
        else {
            return false;
        }
    }
});

Template.link.events({
    'click .delete'() {
        Meteor.call('link.remove', this._parentId, this.url, this.owner);
    },
});
