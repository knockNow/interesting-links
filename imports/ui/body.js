import { Template } from 'meteor/templating';
import { Random } from 'meteor/random';
import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';
import Clipboard from 'clipboard';

import { Categories } from '../api/categories.js';
import './input.js';
import './app-theme.js';
import './body.html';

let showAddLink = false;

Accounts.ui.config({
    passwordSignupFields: 'USERNAME_ONLY'
});

Template.body.onCreated(() => {
    Meteor.subscribe('categories');
});

Template.body.helpers({
    theme() {
        return `theme-${Meteor._localStorage.getItem('app-theme')}`;
    },
    categories() {
        return Categories.find({}, {sort: {createdAt: -1}});
    }
});

Template.body.events({
  'click': function(evt) {
    if (evt.target.id === "addLink") {
      var modale = document.getElementById('showLink')
      modale.style.display = (modale.dataset.toggled ^= 1) ? "flex" : "none";
    }
  }
})

if (Meteor.isClient) {
    Template.body.onRendered(function() {
        var clipboard = new Clipboard('.btn-copy-link');
    });
}
