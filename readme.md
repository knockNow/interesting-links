# interesting links application #

Store interesting links and is available on android (and my website soon). Still under development, it's a meteor app

# Development #

## install & run ##

- `$ npm i`
- `$ meteor`       run
- `$ meteor mongo` database access

## run on android ##

- `$ meteor add-platform android` (To do only once)
- `$ meteor run android-device`

## tests ##

In order to enter in test mode, you'll need to stop the regular app from running, or specify an alternate port with `--port XYZ`

`$ meteor test --driver-package meteortesting:mocha`

## misc informations ##

database is the folder .meteor/local/db/ which is in the .gitignore by default.
You only need to copy/paste this folder to move a db apparently

# Deployment #

## build android app ##

`$ meteor build ../build --server https://interesting-links.luteciacorp.org --directory`

The apk location will be `/build/android/project/build/outputs/apk/release/android-release-unsigned.apk`

## build server (+ front-end website) ##

`$ meteor build --server-only ../build`

Make src.tar.gz to send to the production server

# development with meteor (running cordova) #

## meteor packages ##
`$ meteor add package-name`

## cordova packages in meteor project ##
`$ meteor add cordova:cordova-plugin-...@version`

More explanations: https://guide.meteor.com/mobile.html#cordova-plugins
