import '../imports/ui/body.js';

if(!Meteor._localStorage.getItem('app-theme'))
    Meteor._localStorage.setItem('app-theme', 'night');
